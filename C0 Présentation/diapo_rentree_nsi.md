NSI en classe de première  
========================================================

Une spécialité
----------------------

### Modalités

* 4h par semaine
* Groupes à effectifs réduits
* Accès permanent à l'outil informatique
       
### Les fondements de l'informatique

* **des concepts**  : données et informations, algorithmes, langages
  et programmes, machines 
* **leurs applications** en programmation
* démarche de **projet**

### Préparation aux études supérieures

* Contenus abordés à l'heure actuelle après le BAC
* Evolution prévisible des formations
* Des débouchés très nombreux

Les notions abordées
========================================================

7 thèmes 
----------------------

### Représentation des données : types et valeurs de base

* Le binaire, l'hexadécimal
  
* Les booléens


### Représentation des données : types construits

* Tableaux, p-uplets, dictionnaires...


### Traitement de données en tables

* Recherche et tris des données



![compter en hexadécimal](./fig/Hexadecimal-counting.jpg)  


![circuits et fonctions logiques](./fig/389px-TexasInstruments_7400_chip__view_and_element_placement.jpg)




7 thèmes (suite..)
----------------------

### Interactions entre l’homme et la machine sur le _Web_

* Technologies du web
  
* Réseaux


### Architectures matérielles et systèmes d’exploitation

* Systèmes d'exploitation libres : Linux
  
* Lignes de commande

![HTML5 + CSS3](./fig/html_css.jpg)

![Tux](./fig/tux.png)




7 thèmes (suite..)
----------------------


### Langages et programmation

* Python, Javascript, PHP : points communs-différences
  
* Débogage
  
* Bonnes pratiques en programmation
  

### Algorithmique

* Comment trier des informations efficacement ?
  
* Optimisation des algorithmes


![Langages](./fig/proglanguage.jpg)

![TSP](./fig/car54.jpg)

En fil conducteur
----------------------

### Un langage de programmation privilégié :

* Python


### Aspect culturel : 

* Histoire de l'informatique
  



![ENIAC](./fig/581px-ENIAC-changing_a_tube.jpg)



Modalités de travail
----------------------

### Formats

  * Apport théoriques : présentation des concepts en début de séance

  * Travaux pratiques sur machine
  
  * Activités débranchées

  * __Prises de notes__ en complément des documents fournis

### Espace numérique

* Ressources en ligne : FRAMAGIT 


Outils
----------------------

### Matériel

  * Clé USB personnelle

  * Sauvegardes réguliéres ('H:\\Travail' + clé + ordi perso)
  
  * Classeur + feuilles + pochettes transparentes

  * Fiches méthodes personnelles à réaliser
  
### Logiciels

  * Installation Python 3

  * Editeur privilégié : THONNY 
  

Modalités d'évaluation
----------------------

### Connaissances théoriques

* Interrogations __sans machine__ 
  
* QCM 

### Savoir-faire

* évaluations sur des réalisations individuelles et/ou collectives
  
* TP évalués
  
* Mini-projets


Aspect Pluridisciplinaire
-------------------

### Des liens avec d'autres disciplines

* Mathématiques
  
* Physique
  
* Anglais
  
* et des applications dans de nombreuses autres...



Contrôle continu sur 2 ans
-------------------

### Evaluation des bulletins

* 10 % de la note finale au Bac (5% en 1ère et 5% en Terminale)
  
* Epreuve finale théorique de 2H si spé abandonnée en fin de 1ère (E3C : 30%)

### En Terminale...

* spé de 6H
  
* Evaluations théoriques et pratiques




Bonne "rentrée" !
=================

